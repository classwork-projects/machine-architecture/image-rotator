# Image Rotator

This project required implementing blocked two-dimensional arrays to evaluate the performance of image rotation using three different array-access patterns with different locality properties. Essential concepts that I learned while working on this project included cache analysis and how to write polymorphic code in an array type.  

Usage:
`./ppmtrans -rotate 90 -col-major ./pic.ppm`

Flags:  
`-rotate 90` Rotate image 90 degrees clockwise  
`-rotate 180` Rotate image 180 degrees  
`-rotate 270` Rotate image 270 degrees clockwise (or 90 ccw).
`-rotate 0` Leave the image unchanged  
`-row-major` Copy pixels from the source image using map_row_major  
`-col-major` Copy pixels from the source image using map_col_major  
`-block-major` Copy pixels from the source image using map_block_major    

## Architecture 
**ppmtrans**
- Transform ppm images given through standard input or from a file named on the command line and writes the transformed image to standard output.
- Each pixel in a given image will correspond to a pixel in the rotated image
- Each row in the image from left to right will correspond to an equivalent column in the 90 degree rotated image from top to bottom. And each column in the image from top to bottom will correspond to an equivalent row in the 90 degree rotated image from right to left. 
- Each row in the image from left to right will correspond to an equivalent row in the 180 degree rotated image from left to right. And each column in the image from top to bottom will correspond to an equivalent column in the 180 degree rotated image from bottom to top. 
- For each pixel in a given image at location [i, j], it will exist in a block at an UArray2b_T index [i/blocksize, j/blocksize]. Within that Array_T block the pixel will exist at an Array_T cell index blocksize * (i % blocksize) + j % blocksize.  
<br />

**a2plain**
- Provides an interface for the UArray2_T data structure. Contains method pointers for all a2methods except for block_major mapping.
- Exports a pointer to a struct that stores the method pointers  
<br />

**uarray2b**
- A 2 dimensional Array2b_T array of blocks represented as an UArray2_T, where each block is a fixed ‘blocksize’ * ‘blocksize’ of cells, represented as an Array_T. The 2 dimensional array of blocks is either:
    - The NULL pointer, or
    - A pointer p to a structure containing a pointer to a UArray2_T p->array of blocks, a blocksize which measures the structural length of a block, a width, a height, and the size of each block as sum of all of its cells’ sizes.
        - Either p->array is NULL or p->array points to a single Array_T p->array->block where all of the cells in p->array->block are in nearby memory locations
        - Given the cell index (i, j), blocks can be found in p->array at the block index (i / p.blocksize, j / p.blocksize) and within that block the cell can be found at [p.blocksize * (i % p.blocksize) + j % p.blocksize] where p.blocksize is the block’s width w or      height h where w is always equal to h
        - Given a cell’s p->array block index b and a cell’s block indices (row, col), the cell’s indices (i, j) in the encompassing blocked array are represented as i = b * p.blocksize + row and j = b * p.blocksize + col
        - Unused cells whose index is < p.blocksize but have not been assigned values in a p->array->block should be ignored by mapping functions and are otherwise inaccessible
    and,
    - The NULL pointer represents an empty array.
    - A nonull pointer p represents a blocked two dimensional block array represented by p->array and its subset of p->array->block’s where each p->array->block is represented as a single array.
<br />

## Conclusions  
Timings measured in seconds:  

|   	        | 90 	| 180 	|
|---	        |---	|---	|
| row major 	| 5.16 	| 7.05 	|
| column major 	| 4.86 	| 7.48 	|
| block major 	| 6.55 	| 6.98 	|  

These timings provide some suprising results. It was expected that block major access would have similiar performance for both rotations. However the terrible performance for row major 180 degree rotation is confusing as we predicted this to be the most performance efficient transformation. The good performance of row major 90 degree rotation is also unexpected as the cache hit rate was expected to be lower for this transformation than for block major mapping. We believe these results are caused by not being able to test our block major data structure on larger images, therefore skewing the data. However it does show that block major provides the most consistent performance across transformations.

