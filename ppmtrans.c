#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "assert.h"
#include "a2methods.h"
#include "a2plain.h"
#include "a2blocked.h"
#include "pnm.h"

typedef A2Methods_Array2 A2; // private abbreviation

//Apply function to rotate ppm images 90 degrees
void rotate_90_apply(int col, int row, A2 array, void *elem, void *cl)
{
    //New rotated image
    Pnm_ppm rotated_image = (Pnm_ppm)cl;
    //Current pixel to move
    Pnm_rgb current = (Pnm_rgb)elem;
    //New pixel position
    Pnm_rgb to_swap;
    const struct A2Methods_T *methods = (rotated_image->methods);
    //New pixel column for 90 degree rotation
    int new_col = rotated_image->width - row -1;
    //New pixel row for 90 degree rotation
    int new_row = col;
    //Point to correct position at new row and column
    to_swap = (Pnm_rgb)methods->at(rotated_image->pixels, new_col, new_row);
    //change the value to that of the current element
    *to_swap = *current;
    (void)array;
}

//Apply function to rotate ppm images 180 degrees
void rotate_180_apply(int col, int row, A2 array, void *elem, void *cl)
{
    //New rotated image
    Pnm_ppm rotated_image = (Pnm_ppm)cl;
    //Current pixel to move
    Pnm_rgb current = (Pnm_rgb)elem;
    //New pixel position
    Pnm_rgb to_swap;
    const struct A2Methods_T *methods = (rotated_image->methods);
    //New pixel column for 180 degree rotation
    int new_col = rotated_image->width - col - 1;
    //New pixel row for 180 degree rotation
    int new_row = rotated_image->height - row -1;
    //Point to correct position at new row and column
    to_swap = (Pnm_rgb)methods->at(rotated_image->pixels, new_col, new_row);
    //change the value to that of the current element
    *to_swap = *current;
    (void)array;
}

// Apply function to rotate ppm images 270 degrees
void rotate_270_apply(int col, int row, A2 array, void *elem, void *cl)
{
    //New rotated image
    Pnm_ppm rotated_image = (Pnm_ppm)cl;
    //Current pixel to move
    Pnm_rgb current = (Pnm_rgb)elem;
    //New pixel position
    Pnm_rgb to_swap;
    const struct A2Methods_T *methods = (rotated_image->methods);
    //New pixel column for 270 degree rotation
    int new_col = row;
    //New pixel row for 270 degree rotation
    int new_row = rotated_image->height - col -1;
    //Point to correct position at new row and column
    to_swap = (Pnm_rgb)methods->at(rotated_image->pixels, new_col, new_row);
    //change the value to that of the current element
    *to_swap = *current;
    (void)array;
}

int main(int argc, char *argv[])
{
    int rotation = 0;
    FILE* ppm_file = NULL;
    Pnm_ppm source_img;

    A2Methods_T methods = array2_methods_plain; // default to UArray2 methods
    assert(methods);
    A2Methods_mapfun *map = methods->map_default; // default to best map
    assert(map);

#define SET_METHODS(METHODS, MAP, WHAT) do { \
      methods = (METHODS); \
      assert(methods); \
      map = methods->MAP; \
      if (!map) { \
        fprintf(stderr, "%s does not support " WHAT "mapping\n", argv[0]); \
        exit(1); \
      } \
    } while(0)

    for (int i = 1; i < argc; i++)
    {
        if (!strcmp(argv[i], "-row-major"))
        {
            SET_METHODS(array2_methods_plain, map_row_major, "row-major");
        } else if (!strcmp(argv[i], "-col-major"))
        {
            SET_METHODS(array2_methods_plain, map_col_major, "column-major");
        } else if (!strcmp(argv[i], "-block-major"))
        {
            SET_METHODS(array2_methods_blocked, map_block_major, "block-major");
        } else if (!strcmp(argv[i], "-rotate"))
        {
            assert(i + 1 < argc);
            char *endptr;
            rotation = strtol(argv[++i], &endptr, 10);
            assert(*endptr == '\0'); // parsed all correctly
            assert(rotation == 0 || rotation == 90
                   || rotation == 180 || rotation == 270);
        } else if (*argv[i] == '-')
        {
            fprintf(stderr, "%s: unknown option '%s'\n", argv[0], argv[i]);
            exit(1);
        } else if (strstr(argv[i], ".ppm") != NULL)
        {
            ppm_file = fopen(argv[i], "rb"); // opens file
            if (ppm_file == NULL)
            {
                fprintf(stderr, "%s: Could not open file %s for reading. Closing file and doing nothing.\n",argv[0], argv[1]);
                exit(EXIT_FAILURE);
            }
        } else if (argc - i > 2)
        {
            fprintf(stderr, "Usage: %s [-rotate <angle>] "
                            "[-{row,col,block}-major] [filename]\n", argv[0]);
            exit(1);
        } else {
            fprintf(stderr, "ERROR: Incorrect input format.\n");
            return 1;
        }
    }

    // read from standard in
    if (ppm_file == NULL)
    {
        ppm_file = stdin;
    }
    assert(ppm_file);
    // 2 dimensional array that contains ppm image data
    TRY
            source_img = Pnm_ppmread(ppm_file, methods);

    EXCEPT(Pnm_Badformat)
        fprintf(stderr, "Incorrect format\n");
        exit(1);
    END_TRY;

    if(rotation==0)
    {
        Pnm_ppmwrite(stdout, source_img);
        Pnm_ppmfree(&source_img);
    }

    else if(rotation==90)
    {
        Pnm_ppm rotated_img = (Pnm_ppm) malloc(sizeof(*source_img));
        rotated_img->methods = methods;
        rotated_img->denominator = source_img->denominator;

        //Store array_height and array_width of rotated image, since 90 degrees array_height and array_width are opposite
        rotated_img->height = source_img->width;
        rotated_img->width = source_img->height;

        //Initialize 2d pixel array
        rotated_img->pixels = methods->new(source_img->height,source_img->width, sizeof(struct Pnm_rgb));

        //Map each pixel from source image into the corresponding 90 degree rotated position
        map(source_img->pixels, rotate_90_apply,rotated_img);

        Pnm_ppmwrite(stdout, rotated_img);
        Pnm_ppmfree(&source_img);
        Pnm_ppmfree(&rotated_img);
    }
    else if(rotation==180)
    {
        Pnm_ppm rotated_img = (Pnm_ppm) malloc(sizeof(*source_img));
        rotated_img->methods = methods;
        rotated_img->denominator = source_img->denominator;

        //Store array_height and array_width of rotated image, since 180 degrees array_height and array_width are the same
        rotated_img->height = source_img->height;
        rotated_img->width = source_img->width;

        //Initialize 2d pixel array
        rotated_img->pixels = methods->new(source_img->width,source_img->height, sizeof(struct Pnm_rgb));

        //Map each pixel from source image into the corresponding 180 degree rotated position
        map(source_img->pixels,rotate_180_apply,rotated_img);

        Pnm_ppmwrite(stdout, rotated_img);
        Pnm_ppmfree(&source_img);
        Pnm_ppmfree(&rotated_img);
    }
    else if(rotation==270)
    {
        Pnm_ppm rotated_img = (Pnm_ppm) malloc(sizeof(*source_img));
        rotated_img->methods = methods;
        rotated_img->denominator = source_img->denominator;

        //Store array_height and array_width of rotated image, since 270 degrees array_height and array_width are opposite
        rotated_img->height = source_img->width;
        rotated_img->width = source_img->height;

        //Initialize 2d pixel array
        rotated_img->pixels = methods->new(source_img->height,source_img->width, sizeof(struct Pnm_rgb));

        //Map each pixel from source image into the corresponding 270 degree rotated position
        map(source_img->pixels,rotate_270_apply,rotated_img);

        Pnm_ppmwrite(stdout, rotated_img);
        Pnm_ppmfree(&source_img);
        Pnm_ppmfree(&rotated_img);
    }
    else
    {
        fprintf(stderr, "Not a valid rotation\n");
        exit(1);
    }

    fclose(ppm_file);
}
