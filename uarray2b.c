#include "uarray2b.h"
#include "uarray2.h"
#include <assert.h>
#include "stdlib.h"
#include "stdio.h"
#include "math.h"
#include "mem.h"
#include "array.h"

#define T UArray2b_T

struct T
{
    int array_width;
    int array_height;
    int contents_width;
    int contents_height;
    int size;
    int blocksize;
    UArray2_T main_array;
};


// given a block dimension and blocksize, return the number of blocks per row or per column
int block_count(int array_dimension, int blocksize)
{
    if (array_dimension % blocksize != 0)
    {
        return ((array_dimension / blocksize) + 1);
    } else
    {
        return (array_dimension / blocksize);
    }
}

// returns a pointer to a two dimensional array of width * height Array_T arrays, each of blocksize * blocksize cells
// For each cell in a given UArray2b array at location [i, j], it will exist in a block at an UArray2b_T index [i/blocksize, j/blocksize].
// Within that Array_T block the cell will exist at an Array_T index blocksize * (i % blocksize) + j % blocksize.
T UArray2b_new(int width, int height, int size, int blocksize)
{
    assert((width > 0) && (height > 0) && (blocksize >= 1));

    T array2b = NEW(array2b);
    array2b->array_width = width; // width of the two dimensional array
    array2b->array_height = height; // height of the two dimensional array
    array2b->size = size;
    array2b->blocksize = blocksize;
    array2b->contents_width = block_count(width, blocksize); // number of blocks in a row
    array2b->contents_height = block_count(height, blocksize); // number of blocks in a column
    array2b->main_array = UArray2_new(array2b->contents_width, array2b->contents_height, sizeof(UArray2_T));

    // allocates an Array_T for each block in the two dimensional main_array
    for (int r = 0; r < array2b->contents_height; r++)
    {
        for (int c = 0; c < array2b->contents_width; c++)
        {
            Array_T *new_array = UArray2_at(array2b->main_array, c, r);
            *new_array = Array_new((blocksize * blocksize), array2b->size);
        }
    }

    return array2b;
}

// returns a pointer to a two dimensional array of width * height Array_T arrays, each of the largest blocksize possible
T UArray2b_new_64K_block(int width, int height, int size)
{
    assert((width > 0) && (height > 0));

    int blocksize;
    // if cell is larger than 64KB
    if (size > 64000)
    {
        blocksize = 1;
    } else
    {
        blocksize = sqrt(64 / size);
    }

    return UArray2b_new(width, height, size, blocksize);
}

// given a pointer to an UArray2b_T array, frees the array and the array pointer
void UArray2b_free(T *array2b)
{
    // for each block in the main_array
    for (int row = 0; row < (*array2b)->contents_height; row++)
    {
        for (int col = 0; col < (*array2b)->contents_width; col++)
        {
            UArray2_T *temp_block = UArray2_at((*array2b)->main_array, col, row);
            UArray2_free(temp_block);
        }
    }

    // free UArray2 array storing the blocks
    UArray2_free(&((*array2b)->main_array));
    // fre the UArray2b_T struct
    FREE(*array2b);
}

// given an UArray2b_T object, returns the width of the two dimensional blocked array
int UArray2b_width(T array2b)
{
    return array2b->array_width;
}

// given an UArray2b_T object, returns the height of the two dimensional blocked array
int UArray2b_height(T array2b)
{
    return array2b->array_height;
}

// given an UArray2b_T object, returns the size of the two dimensional blocked array's elements (blocks)
int UArray2b_size(T array2b)
{
    return array2b->size;
}

// given an UArray2b_T object, returns the blocksize of the two dimensional blocked array's blocks
int UArray2b_blocksize(T array2b)
{
    return array2b->blocksize;
}

// given an UArray2b_T object, returns a pointer to the element stored at index [i, j]
void *UArray2b_at(T array2b, int i, int j)
{
    int b_size = array2b->blocksize;

// get the block that contains cell [i, j]
    Array_T *temp_block = UArray2_at(array2b->main_array, i / b_size, j / b_size);

// block index of the Array_T block array that stores element [i, j]
    int block_index = b_size * (j % b_size) + (i % b_size);

    return Array_get(*temp_block, block_index);
}

// given an UArray2b_T object, calls a given apply() function with a passed pointer to a closure on every cell in the
// two dimensional array
void UArray2b_map(T array2b, void apply(int i, int j, T array2b, void *elem, void *cl), void *cl)
{
// for each block in the two dimensional array
    for (int array_row = 0; array_row < array2b->contents_height; array_row++)
    {
        for (int array_col = 0; array_col < array2b->contents_width; array_col++)
        {
            Array_T *block = UArray2_at(array2b->main_array, array_col, array_row);
            int b_size = array2b->blocksize;

            // get the indices of the first element of each block
            int block_row = array_row * b_size;
            int block_col = array_col * b_size;
            // for each cell in the current block
            for (int r = block_row; r < block_row + b_size; r++)
            {
                for (int c = block_col; c < block_col + b_size; c++)
                {
                    // if visiting a usable array area
                    if ((r < array2b->array_height) && (c < array2b->array_width))
                    {
                        int cell_index = (r % b_size) + (c % b_size) * b_size;
                        apply(c, r, array2b, Array_get(*block, cell_index), cl);
                    }
                }
            }
        }
    }
}
